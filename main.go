package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"

	color "github.com/snaggen/aurora"
	kb "github.com/snaggen/keyboard"
)

func main() {
	excludeSpecial := flag.Bool("s", false, "Exclude special keys")
	inkludeArrows := flag.Bool("a", false, "Inklude ArrowKeys")
	flag.Parse()

	lookup := make(map[kb.Key]string)

	if !*excludeSpecial {
		//0xFFF4 is F12, but since F11 is Fullscreen in Linux,
		//we just go to F10
		for i := uint16(0xFFFF); i >= 0xFFF6; i-- {
			lookup[kb.Key(i)] = fmt.Sprintf("F%d", (0xFFFF-i)+1)
		}
		lookup[kb.KeyInsert] = "Insert"
		lookup[kb.KeyDelete] = "Delete"
		lookup[kb.KeyPgup] = "PgUp"
		lookup[kb.KeyPgdn] = "PgDn"
		lookup[kb.KeyEnter] = "Enter"
		lookup[kb.KeyBackspace2] = "Backspace"
		lookup[kb.KeyTab] = "Tab"
		lookup[kb.KeySpace] = "Space"
		if *inkludeArrows {
			lookup[kb.KeyHome] = "Home"
			lookup[kb.KeyEnd] = "End"
			lookup[kb.KeyArrowUp] = "\u2191"
			lookup[kb.KeyArrowDown] = "\u2193"
			lookup[kb.KeyArrowLeft] = "\u2190"
			lookup[kb.KeyArrowRight] = "\u2192"
		}
	}

	chars := []rune("`~!@#$%^&*()-_=+[]{}\\|;:'\",<.>/?")
	var specialChars []kb.Key
	for k := range lookup {
		specialChars = append(specialChars, k)
	}
	rand.Seed(time.Now().UTC().UnixNano())

	err := kb.Open()
	if err != nil {
		panic(err)
	}
	defer kb.Close()

	noChar := 0
	errors, total := 0, 0
	var elapsed time.Duration
	var average int64
	var prevRnd, rnd int
	noChars := len(chars) + len(specialChars)

	printInitialPrompt()

	progStart := time.Now()
	for {
		start := time.Now()
		var chRune rune
		var chKey kb.Key

		for {
			rnd = rand.Intn(noChars)
			if rnd != prevRnd {
				break
			}
		}
		prevRnd = rnd

		var challenge string
		if rnd >= len(chars) {
			chRune = 0x00
			chKey = specialChars[rnd-len(chars)]
			challenge = lookup[chKey]
		} else {
			chRune = chars[rnd]
			chKey = 0x0000
			challenge = string(chRune)
		}
		noChar++
		for {
			fmt.Printf(" %v ", color.Yellow(challenge))
			char, key, err := kb.GetKey()
			total++
			if err != nil {
				fmt.Printf(" ERROR: %v\n", err)
				continue
			}
			if char == 0x00 && (key == kb.KeyCtrlC || key == kb.KeyCtrlD) {
				fmt.Printf("\n\nYou typed %v characters with an error percent of %3v%% and %0.4vs mean time\n", noChar-1, int((float64(errors)/float64(total))*100), time.Duration(average))
				return
			}
			elapsed = time.Since(start)
			average = time.Since(progStart).Nanoseconds() / int64(noChar)
			if char == 0x00 && key == kb.KeyEsc {
				fmt.Println(color.Cyan(" Skip!"))
				printPrompt(elapsed, average, errors, total)
				break
			} else if char == chRune && key == chKey {
				fmt.Println(color.Green(" Ok!"))
				printPrompt(elapsed, average, errors, total)
				break
			} else {
				var typed string
				if char == 0x00 {
					if _, ok := lookup[key]; ok {
						typed = lookup[key]
					} else {
						typed = fmt.Sprintf("special character 0x%04x", key)
					}
				} else {
					typed = string(char)
				}
				fmt.Printf(" %v (you typed %v)\n", color.Red("Wrong!"), typed)
				errors++
				printPrompt(elapsed, average, errors, total)
			}
		}
	}
}

func printPrompt(elapsed time.Duration, average int64, errors int, total int) {
	fmt.Printf("%3v%% %0.4vs %0.4vs\t\t", int((float64(errors)/float64(total))*100), time.Duration(average), elapsed)
}
func printInitialPrompt() {
	fmt.Printf(" --%% -.--s -.--s\t\t")
}
