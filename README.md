# keytrainer
Simple typing training program written in Go, that focuses on non-alphanumerical characters.

To run it you must first fetch the dependencies

`go get -u github.com/snaggen/keyboard`

`go get -u github.com/snaggen/aurora`

After that you may run it using

`go run main.go`

you may use the flag -s if you want to skip F-Keys and some special characters

# Usage

Esc - Will skip the current character

Ctrl-C or Ctrl-D will exit
